using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sentry;

namespace GlitchTip
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                using (SentrySdk.Init("https://f4ebeef192c74276b8c48bf6ae54a64c@glitchtip.cantaluppi.app/1"))
                {
                    CreateHostBuilder(args).Build().Run();
                }
            }
            catch
            {
                CreateHostBuilder(args).Build().Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}