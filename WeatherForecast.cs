using Sentry;
using System;

namespace GlitchTip
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        //public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
        public int TemperatureF {
            get
            {
                try
                {
                    int x = 1;
                    if (true)
                    {
                        x = 0;
                    }
                    return 1 / x;
                }
                catch (Exception err)
                {
                    SentrySdk.CaptureException(err);
                }
                return 0;
            }
        }
        
        public string Summary { get; set; }
    }
}
